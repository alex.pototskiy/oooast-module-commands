<?php
/**
 * PHP version 7.1
 * Console command to clean up unused catalog images
 *
 * @category Helper commands
 * @package  OooAst_Commands
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 12.05.2019
 * Time: 9:14
 */
declare(strict_types=1);

namespace OooAst\Commands\Console\Command;

use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\FileSystemException;
use OooAst\Commands\Console\Helper\FileAndDirectoryCleaner;
use OooAst\Commands\Traits\ConsoleMessageDecorator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CleanupCatalogImages
 *
 * @package OooAst\Commands\Console\Command
 */
class CleanupCatalogImages extends Command
{
    use ConsoleMessageDecorator;

    const CMD_NAME = 'oooast:media:clean';
    const OPT_MEDIA_TYPE = 'type';
    const OPT_DRY_RUN = 'dry-run';

    const CATEGORY_IMAGES_PATH = 'catalog/category';
    const PRODUCT_IMAGES_PATH = 'catalog/product';
    const PAGE_SIZE = 100;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryList;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var OutputInterface
     */
    private $output;
    /**
     * Count of removed files
     *
     * @var int
     */
    private $removedFilesCount = 0;
    /**
     * Count of removed directories
     *
     * @var int
     */
    private $removedDirsCount = 0;
    /**
     * Count of removed gallery table items
     *
     * @var int
     */
    private $removedGalleryItems = 0;
    /**
     * @var FileAndDirectoryCleaner
     */
    private $cleaner;
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;
    /**
     * @var string
     */
    private $mediaGalleryTableName;
    /**
     * @var string
     */
    private $mediaGalleryValueTableName;
    /**
     * @var string
     */
    private $mediaGalleryEntityToValueTableName;

    /**
     * CleanupCatalogImages constructor.
     *
     * @param CategoryListInterface $categoryList
     * @param ProductRepositoryInterface $productRepository
     * @param FileAndDirectoryCleaner $cleaner
     * @param SearchCriteriaBuilder $criteriaBuilder
     * @param ResourceConnection $resourceConnection
     * @param ResourceModelFactory $resourceModelFactory
     * @param string|null $name
     */
    public function __construct(
        CategoryListInterface $categoryList,
        ProductRepositoryInterface $productRepository,
        FileAndDirectoryCleaner $cleaner,
        SearchCriteriaBuilder $criteriaBuilder,
        ResourceConnection $resourceConnection,
        ResourceModelFactory $resourceModelFactory,
        string $name = null
    ) {
        $this->categoryList = $categoryList;
        $this->productRepository = $productRepository;
        $this->cleaner = $cleaner;
        $this->searchCriteriaBuilder = $criteriaBuilder;
        $this->connection = $resourceConnection->getConnection();
        $resource = $resourceModelFactory->create();
        $this->mediaGalleryTableName = $resource->getTable('catalog_product_entity_media_gallery');
        $this->mediaGalleryValueTableName = $resource->getTable(
            'catalog_product_entity_media_gallery_value'
        );
        $this->mediaGalleryEntityToValueTableName = $resource->getTable(
            'catalog_product_entity_media_gallery_value_to_entity'
        );
        parent::__construct($name);
    }

    /**
     * Configure command name and options
     */
    protected function configure()
    {
        $this->setName(self::CMD_NAME)
            ->setDescription('Clean up unused catalog images')
            ->addOption(
                self::OPT_MEDIA_TYPE,
                't',
                InputOption::VALUE_REQUIRED,
                'media type: product, category or both',
                'both'
            )
            ->addOption(
                self::OPT_DRY_RUN,
                'd',
                InputOption::VALUE_OPTIONAL,
                'dry run - no real removing',
                false
            );
        parent::configure();
    }

    /**
     * Execute command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $dryRun = (bool)($input->getOption(self::OPT_DRY_RUN) !== false);
        try {
            if (in_array($input->getOption(self::OPT_MEDIA_TYPE), ['category', 'both'])) {
                $this->cleanupCategoryMedia($dryRun);
                $this->removedDirsCount += count(
                    $this->cleaner->removeEmptyDir(self::CATEGORY_IMAGES_PATH)
                );
            }
            if (in_array($input->getOption(self::OPT_MEDIA_TYPE), ['product', 'both'])) {
                $this->cleanupProductMedia($dryRun);
                $this->removedDirsCount += count(
                    $this->cleaner->removeEmptyDir(self::PRODUCT_IMAGES_PATH)
                );
                $this->removedGalleryItems = $this->removeUnusedGalleryItems($dryRun);
            }
            $output->writeln($this->removedFilesCount . ' files have been removed.');
            $output->writeln($this->removedDirsCount . ' directories have been removed.');
            $output->writeln($this->removedGalleryItems . ' gallery items have been removed.');
        } catch (FileSystemException $e) {
            $output->writeln($this->decorateAsError('Something went wrong: error:' . $e->getMessage()));
            return 1;
        }
        return 0;
    }

    /**
     * Cleanup unused category images
     *
     * @param bool $dryRun
     *
     * @throws FileSystemException
     */
    private function cleanupCategoryMedia(bool $dryRun)
    {
        if ($this->cleaner->isDirectoryExist(self::CATEGORY_IMAGES_PATH)) {
            $used = $this->collectCategoryUsedImages();
            foreach ($this->cleaner->readDirContent(self::CATEGORY_IMAGES_PATH) as $dirOrFile) {
                $files = $this->cleaner->removeUnusedFiles($dirOrFile, $used, $dryRun);
                $this->removedFilesCount += count($files);
                if ($dryRun) {
                    foreach ($files as $file) {
                        $this->output->writeln('File: ' . $file . ' will be removed');
                    }
                }
            };
        }
    }

    /**
     * Collect images that is used by categories
     *
     * @return array
     */
    private function collectCategoryUsedImages(): array
    {
        $images = [];
        $page = 0;
        do {
            ++$page;
            $searchCriteria = $this->searchCriteriaBuilder
                ->setPageSize(self::PAGE_SIZE)
                ->setCurrentPage($page)
                ->create();
            $result = $this->categoryList->getList($searchCriteria);
            foreach ($result->getItems() as $category) {
                if ($category instanceof Category) {
                    $image = $category->getData('image');
                    if ($image !== null && !empty($image)) {
                        $images[] = 'catalog/category/' . $image;
                    }
                }
            }
        } while ($result->getTotalCount() == self::PAGE_SIZE);
        return $images;
    }

    /**
     * Cleanup product images
     *
     * @param bool $dryRun
     *
     * @throws FileSystemException
     */
    private function cleanupProductMedia(bool $dryRun)
    {
        if ($this->cleaner->isDirectoryExist(self::PRODUCT_IMAGES_PATH)) {
            $usedImages = $this->collectProductUsedImages();
            foreach ($this->cleaner->readDirContent(self::PRODUCT_IMAGES_PATH) as $dirOrFile) {
                $files = $this->cleaner->removeUnusedFiles($dirOrFile, $usedImages, $dryRun);
                $this->removedFilesCount += count($files);
                if ($dryRun) {
                    foreach ($files as $file) {
                        $this->output->writeln('File: ' . $file . ' will be removed');
                    }
                }
            }
        }
    }

    /**
     * Collection product images
     *
     * @return array
     */
    private function collectProductUsedImages(): array
    {
        $images = [];
        $page = 0;
        do {
            ++$page;
            $criteria = $this->searchCriteriaBuilder
                ->setPageSize(self::PAGE_SIZE)
                ->setCurrentPage($page)
                ->create();
            $result = $this->productRepository->getList($criteria);
            foreach ($result->getItems() as $product) {
                if ($product instanceof Product) {
                    foreach ($product->getMediaGallery('images') as $image) {
                        $images[] = self::PRODUCT_IMAGES_PATH . $image['file'];
                    }
                }
            }
        } while ($result->getTotalCount() == self::PAGE_SIZE);
        return $images;
    }

    /**
     * Remove items from product media gallery
     *
     * @param bool $dryRun
     *
     * @return int
     */
    private function removeUnusedGalleryItems(bool $dryRun): int
    {
        $select = $this->connection->select()
            ->from(
                ['mg' => $this->mediaGalleryTableName],
                ['value_id' => 'mg.value_id']
            )
            ->joinLeft(
                ['mgv' => $this->mediaGalleryValueTableName],
                '(mg.value_id = mgv.value_id)',
                ''
            )
            ->where(
                '(mgv.value_id is null)'
            );
        $count = count($this->connection->fetchAll($select));
        if (!$dryRun) {
            $deleteSql = $this->connection->deleteFromSelect($select, 'mg');
            $this->connection->query($deleteSql);
        }
        return $count;
    }
}
