<?php
/**
 * PHP version 7.1
 * Export CLI interface
 *
 * @category ImportExport
 * @package  OooAst_ConsoleCommands
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 05.12.2018
 * Time: 14:37
 */
declare(strict_types=1);

namespace OooAst\Commands\Console\Command;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\ImportExport\Model\Export;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\Commands\Traits\ConsoleMessageDecorator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Cli command to export data
 *
 * @package OooAst\Commands\Console\Command
 */
class ExportCommand extends Command
{
    use ConsoleMessageDecorator;

    const CMD_NAME = 'oooast:export';

    const OPT_TYPE = 'type';
    const OPT_TARGET_DIR = 'dir';
    const OPT_FILE_FORMAT = 'format';

    /**
     * Csv exporter factory
     *
     * @var Export
     */
    private $exporter;
    /**
     * Magento store manager
     *
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * Magento file system
     *
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Export\Config
     */
    private $exportConfig;

    /**
     * CategoryExportCommand constructor.
     *
     * @param Export $exporter The magento export
     * @param StoreManagerInterface $storeManager The store manager
     * @param Filesystem $filesystem The file system
     * @param Export\Config $config
     */
    public function __construct(
        Export $exporter,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        Export\Config $config
    ) {
        $this->exporter = $exporter;
        $this->storeManager = $storeManager;
        $this->filesystem = $filesystem;
        $this->exportConfig = $config;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::CMD_NAME)
            ->setDescription('Export entities to CSV data')
            ->addOption(
                self::OPT_TYPE,
                "-t",
                InputOption::VALUE_REQUIRED,
                'type of data to export: ' . implode(
                    ', ',
                    array_keys($this->exportConfig->getEntities())
                ),
                null
            )
            ->addOption(
                self::OPT_TARGET_DIR,
                '-d',
                InputOption::VALUE_REQUIRED,
                'target directory to write csv data file, relative to magento var',
                ''
            )
            ->addOption(
                self::OPT_FILE_FORMAT,
                '-f',
                InputOption::VALUE_REQUIRED,
                'file format: ' . implode(
                    ', ',
                    array_keys($this->exportConfig->getFileFormats())
                ),
                null
            );
        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entity = $input->getOption(self::OPT_TYPE);
        $format =$input->getOption(self::OPT_FILE_FORMAT);
        $dirName = $input->getOption(self::OPT_TARGET_DIR);
        try {
            if (substr($dirName, -1) != '/') {
                $dirName = $dirName . '/';
            }
            $this->exporter->setData(
                [
                    'entity' => $entity,
                    'file_format' => $format,
                    'fields_enclosure' => 0,
                    'export_filter' => []
                ]
            );
            $data = $this->exporter->export();
            $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR)
                ->writeFile($dirName . $this->exporter->getFileName(), $data);
            $output->writeln('var/' . $dirName . $this->exporter->getFileName() . ' created.');
        } catch (Exception $e) {
            $output->writeln($this->decorateAsError('Something went wrong: error:' . $e->getMessage()));
            return 1;
        }
        return 0;
    }
}
