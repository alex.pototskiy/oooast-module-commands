<?php
/**
 * PHP version 7.1
 *
 * Import commands CLI
 *
 * @category ImportExport
 * @package  OooAst_Commands
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 16.04.2019
 * Time: 18:14
 */
declare(strict_types=1);

namespace OooAst\Commands\Console\Command;

use Exception;
use Magento\Framework\Filesystem;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Config;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\Source\Import\AbstractBehavior;
use Magento\Store\Api\StoreManagementInterface;
use OooAst\Commands\Traits\ConsoleMessageDecorator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CLI import command
 *
 * @package OooAst\Commands\Console\Command
 */
class ImportCommand extends Command
{
    use ConsoleMessageDecorator;

    const CMD_NAME = 'oooast:import';
    const OPT_TYPE = 'type';
    const OPT_IMPORT_FILE = 'file';
    const OPT_BEHAVIOR = 'behavior';
    const OPT_IMAGE_DIR = 'image-dir';
    const OPT_FIELD_SEPARATOR = 'field-separator';
    const OPT_MULTI_VALUE_SEPARATOR = 'multi-value-separator';
    const OPT_EMPTY_ATTR_VALUE = 'empty-attr-value';
    const OPT_FIELD_ENCLOSURE = 'field-enclosure';

    /**
     * @var Import
     */
    private $importer;
    /**
     * @var StoreManagementInterface
     */
    private $storeManager;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * CSV source factory
     *
     * @var Import\Source\CsvFactory
     */
    private $csvFactory;
    /**
     * @var Config
     */
    private $importConfig;

    /**
     * ImportCommand constructor.
     *
     * @param Import $importer
     * @param Import\Source\CsvFactory $csvFactory
     * @param StoreManagementInterface $storeManagement
     * @param Filesystem $filesystem
     * @param Config $importConfig
     */
    public function __construct(
        Import $importer,
        Import\Source\CsvFactory $csvFactory,
        StoreManagementInterface $storeManagement,
        Filesystem $filesystem,
        Config $importConfig
    ) {
        $this->csvFactory = $csvFactory;
        $this->importer = $importer;
        $this->storeManager = $storeManagement;
        $this->filesystem = $filesystem;
        $this->importConfig = $importConfig;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName(self::CMD_NAME)
            ->setDescription("Import entities from CSV file")
            ->addOption(
                self::OPT_TYPE,
                '-t',
                InputOption::VALUE_REQUIRED,
                'type of data to export: ' .
                implode(', ', array_keys($this->importConfig->getEntities())),
                null
            )
            ->addOption(
                self::OPT_BEHAVIOR,
                '-b',
                InputOption::VALUE_OPTIONAL,
                'import behavior: ' . $this->behaviorHelp(),
                null
            )
            ->addOption(
                self::OPT_IMPORT_FILE,
                '-f',
                InputOption::VALUE_REQUIRED,
                'import file name',
                null
            )
            ->addOption(
                self::OPT_IMAGE_DIR,
                '-i',
                InputOption::VALUE_REQUIRED,
                'image base directory',
                'var/tmp'
            )
            ->addOption(
                self::OPT_EMPTY_ATTR_VALUE,
                null,
                InputOption::VALUE_OPTIONAL,
                'empty attribute value',
                '__EMPTY__VALUE__'
            )
            ->addOption(
                self::OPT_FIELD_ENCLOSURE,
                null,
                InputOption::VALUE_NONE,
                'field enclosure or not'
            )
            ->addOption(
                self::OPT_FIELD_SEPARATOR,
                null,
                InputOption::VALUE_OPTIONAL,
                'field separator symbol',
                ','
            )
            ->addOption(
                self::OPT_MULTI_VALUE_SEPARATOR,
                null,
                InputOption::VALUE_OPTIONAL,
                'multiple value separator',
                ','
            );
        parent::configure();
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (($entityType = $this->getEntityType($input)) == null) {
            $output->writeln($this->decorateAsError('Unsupported import type'));
            return 1;
        };
        if (($behavior = $this->getAndCheckBehavior($input, $entityType)) == null) {
            $output->writeln($this->decorateAsError('Unsupported import behavior'));
            return 1;
        }
        $importFile = $input->getOption(self::OPT_IMPORT_FILE);
        $fieldEnclosure = $input->getOption(self::OPT_FIELD_ENCLOSURE) != null;
        $fieldSeparator = $input->getOption(self::OPT_FIELD_SEPARATOR);
        $valueSeparator = $input->getOption(self::OPT_MULTI_VALUE_SEPARATOR);
        $emptyValue = $input->getOption(self::OPT_EMPTY_ATTR_VALUE);
        $this->importer->setData(
            [
                'entity' => $entityType,
                'behavior' => $behavior,
                Import::FIELD_NAME_VALIDATION_STRATEGY =>
                    ProcessingErrorAggregatorInterface::VALIDATION_STRATEGY_STOP_ON_ERROR,
                Import::FIELD_NAME_ALLOWED_ERROR_COUNT => 10,
                Import::FIELDS_ENCLOSURE => $fieldEnclosure,
                Import::FIELD_FIELD_SEPARATOR => $fieldSeparator,
                Import::FIELD_FIELD_MULTIPLE_VALUE_SEPARATOR => $valueSeparator,
                Import::FIELD_EMPTY_ATTRIBUTE_VALUE_CONSTANT => $emptyValue,
                Import::FIELD_NAME_SOURCE_FILE => $importFile,
                Import::FIELD_NAME_IMG_FILE_DIR => $input->getOption(self::OPT_IMAGE_DIR)
            ]
        );
        $output->writeln(dirname($importFile));
        $output->writeln(basename($importFile));
        $importFileDir = $this->filesystem->getDirectoryReadByPath(dirname($importFile));
        try {
            $source = $this->csvFactory->create(
                [
                    'file' => basename($importFile),
                    'directory' => $importFileDir
                ]
            );
            $this->importer->validateSource($source);
            if ($this->importer->getErrorAggregator()->getErrorsCount() > 0) {
                $output->writeln($this->decorateAsError('Validation is unsuccessful'));
                foreach ($this->importer->getErrorAggregator()->getAllErrors() as $error) {
                    $output->writeln(
                        '[ERROR] : ' . $error->getErrorMessage() . ' in row ' . $error->getRowNumber()
                    );
                }
                return 1;
            }
            $this->importer->importSource();
        } catch (Exception $e) {
            $output->writeln($this->decorateAsError('Something went wrong: error:' . $e->getMessage()));
            return 1;
        }
        return 0;
    }

    /**
     * Get entity type to import
     *
     * @param InputInterface $input
     *
     * @return string|null
     */
    private function getEntityType(InputInterface $input): ?string
    {
        $entityType = $input->getOption(self::OPT_TYPE);
        if (array_key_exists($entityType, $this->importConfig->getEntities())) {
            return $entityType;
        } else {
            return null;
        }
    }

    private function getAndCheckBehavior(InputInterface $input, string $entityType): ?string
    {
        $behavior = $input->getOption(self::OPT_BEHAVIOR);
        $config = $this->importConfig->getEntities()[$entityType];
        /** @var AbstractBehavior $behaviorModel */
        $behaviorModel = new $config['behaviorModel']();
        if (!array_key_exists($behavior, $behaviorModel->toArray())) {
            $behavior = null;
        }
        unset($behaviorModel);
        return $behavior;
    }

    /**
     * Get import behavior
     *
     * @param InputInterface $input
     *
     * @return string|null
     */
    private function getBehavior(InputInterface $input): ?string
    {
        $behavior = null;
        switch ($input->getOption(self::OPT_BEHAVIOR)) {
            case 'add-update':
                $behavior = Import::BEHAVIOR_ADD_UPDATE;
                break;
            case 'replace':
                $behavior = Import::BEHAVIOR_REPLACE;
                break;
            case 'append':
                $behavior = Import::BEHAVIOR_APPEND;
                break;
            case 'delete':
                $behavior = Import::BEHAVIOR_DELETE;
                break;
            case 'custom':
                $behavior = Import::BEHAVIOR_CUSTOM;
                break;
            default:
                return null;
        }
        return $behavior;
    }

    private function behaviorHelp(): string
    {
        $help = [];
        $entities = $this->importConfig->getEntities();
        foreach ($entities as $entity => $config) {
            /** @var AbstractBehavior $behavior */
            $behavior = new $config['behaviorModel']();
            $options = array_keys($behavior->toArray());
            $help[] = sprintf(
                '%s(%s)',
                $entity,
                implode(', ', $options)
            );
            unset($behavior);
        }
        return implode(', ', $help);
    }
}
