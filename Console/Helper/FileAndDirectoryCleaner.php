<?php
/**
 * PHP version 7.1
 * Helper to clean files and directory
 *
 * @category Helper commands
 * @package  OooAst_Commands
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 13.05.2019
 * Time: 9:25
 */

declare(strict_types=1);

namespace OooAst\Commands\Console\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;

/**
 * File and directory cleaner
 *
 * @package OooAst\Commands\Console\Helper
 */
class FileAndDirectoryCleaner
{
    const CATEGORY_IMAGES_PATH = 'catalog/category';
    const PRODUCT_IMAGES_PATH = 'catalog/product';

    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Filesystem\Directory\ReadInterface
     */
    private $mediaReadDir;
    /**
     * @var Filesystem\Directory\WriteInterface
     */
    private $mediaWriteDir;

    /**
     * FileAndDirectoryCleaner constructor.
     *
     * @param Filesystem $filesystem
     *
     * @throws FileSystemException
     */
    public function __construct(
        Filesystem $filesystem
    ) {
        $this->filesystem = $filesystem;
        $this->mediaReadDir = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $this->mediaWriteDir = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * Remove unused files
     *
     * @param string $dirOrFile
     * @param array $used
     * @param bool $dryRun
     *
     * @return string[]
     * @throws FileSystemException
     */
    public function removeUnusedFiles(string $dirOrFile, array $used, bool $dryRun): array
    {
        $removedFiles = [];
        if ($this->mediaReadDir->isFile($dirOrFile)) {
            if (!in_array($dirOrFile, $used)) {
                if ($dryRun) {
                    $removedFiles[] = $dirOrFile;
                } else {
                    $this->mediaWriteDir->delete($dirOrFile);
                    $removedFiles[] = $dirOrFile;
                }
            }
        } else {
            foreach ($this->mediaReadDir->read($dirOrFile) as $item) {
                $removedFiles = array_merge(
                    $removedFiles,
                    $this->removeUnusedFiles($item, $used, $dryRun)
                );
            }
        }
        return $removedFiles;
    }

    /**
     * Remove empty directories excluding root one
     *
     * @param string $path
     * @param bool $isRoot
     *
     * @return string[]
     * @throws FileSystemException
     */
    public function removeEmptyDir(string $path, bool $isRoot = true): array
    {
        $removedDirs = [];
        if (!$this->mediaReadDir->isDirectory($path)) {
            return $removedDirs;
        }
        $dirContent = $this->mediaReadDir->read($path);
        if (empty($dirContent) && !$isRoot) {
            $this->mediaWriteDir->delete($path);
            $removedDirs[] = $path;
        } elseif (!empty($dirContent)) {
            foreach ($dirContent as $child) {
                if ($this->mediaReadDir->isDirectory($child)) {
                    $removedDirs = array_merge(
                        $removedDirs,
                        $this->removeEmptyDir($child, false)
                    );
                }
            }
            $dirContent = $this->mediaReadDir->read($path);
            if (empty($dirContent) && !$isRoot) {
                $this->mediaWriteDir->delete($path);
                $removedDirs[] = $path;
            }
        }
        return $removedDirs;
    }

    /**
     * Check if directory exists
     *
     * @param string $path
     *
     * @return bool
     */
    public function isDirectoryExist(string $path): bool
    {
        return $this->mediaReadDir->isExist($path);
    }

    /**
     * Read directory content
     *
     * @param string $path
     *
     * @return array
     */
    public function readDirContent(string $path): array
    {
        return $this->mediaReadDir->read($path);
    }
}
