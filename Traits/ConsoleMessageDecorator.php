<?php
/**
 * PHP version 7.1
 * Console message decorator
 *
 * @category Commands
 * @package  OooAst_Commands
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 24.04.2019
 * Time: 9:37
 */

namespace OooAst\Commands\Traits;

/**
 * Console message decorator trait
 *
 * @package OooAst\Commands\Traits
 */
trait ConsoleMessageDecorator
{
    /**
     * Decorate console message as error one
     *
     * @param string $message The message to decorate
     *
     * @return string
     */
    public function decorateAsError(string $message)
    {
        return html_entity_decode(
            '&lt;error&gt;' . $message . '&lt;/error&gt;'
        );
    }
}
